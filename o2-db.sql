CREATE DATABASE  IF NOT EXISTS `o2_database` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `o2_database`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: o2_database
-- ------------------------------------------------------
-- Server version	5.6.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `answer_clue_mapping`
--

DROP TABLE IF EXISTS `answer_clue_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answer_clue_mapping` (
  `CLUE_DATA_Clue_ID` int(11) NOT NULL,
  `CLUE_ANSWER_Ans_ID` int(11) NOT NULL,
  PRIMARY KEY (`CLUE_DATA_Clue_ID`,`CLUE_ANSWER_Ans_ID`),
  KEY `ANSWER_CLUE_MAPPING_CLUE_ANSWER` (`CLUE_ANSWER_Ans_ID`),
  CONSTRAINT `ANSWER_CLUE_MAPPING_CLUE_ANSWER` FOREIGN KEY (`CLUE_ANSWER_Ans_ID`) REFERENCES `clue_answer` (`Ans_ID`),
  CONSTRAINT `ANSWER_CLUE_MAPPING_CLUE_DATA` FOREIGN KEY (`CLUE_DATA_Clue_ID`) REFERENCES `clue_data` (`Clue_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answer_clue_mapping`
--

LOCK TABLES `answer_clue_mapping` WRITE;
/*!40000 ALTER TABLE `answer_clue_mapping` DISABLE KEYS */;
INSERT INTO `answer_clue_mapping` VALUES (99,1),(100,2),(101,3),(102,6);
/*!40000 ALTER TABLE `answer_clue_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `answer_geo_map`
--

DROP TABLE IF EXISTS `answer_geo_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answer_geo_map` (
  `Ans_Geo_ID` int(11) NOT NULL AUTO_INCREMENT,
  `User_ID` int(11) NOT NULL,
  `Lat` int(11) NOT NULL,
  `Long` int(11) NOT NULL,
  PRIMARY KEY (`Ans_Geo_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answer_geo_map`
--

LOCK TABLES `answer_geo_map` WRITE;
/*!40000 ALTER TABLE `answer_geo_map` DISABLE KEYS */;
INSERT INTO `answer_geo_map` VALUES (1,1,1,1),(2,2,2,2),(3,3,3,3);
/*!40000 ALTER TABLE `answer_geo_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `answer_image`
--

DROP TABLE IF EXISTS `answer_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answer_image` (
  `CLUE_ANSWER_Ans_ID` int(11) NOT NULL,
  `Clue_Img` blob NOT NULL,
  KEY `ANSWER_IMAGE_CLUE_ANSWER` (`CLUE_ANSWER_Ans_ID`),
  CONSTRAINT `ANSWER_IMAGE_CLUE_ANSWER` FOREIGN KEY (`CLUE_ANSWER_Ans_ID`) REFERENCES `clue_answer` (`Ans_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answer_image`
--

LOCK TABLES `answer_image` WRITE;
/*!40000 ALTER TABLE `answer_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `answer_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `city_code`
--

DROP TABLE IF EXISTS `city_code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city_code` (
  `City_Code` int(11) NOT NULL AUTO_INCREMENT,
  `City_Name` varchar(100) NOT NULL,
  `Cntry_Code` int(11) NOT NULL,
  PRIMARY KEY (`City_Code`),
  KEY `CITY_CODE_FK` (`Cntry_Code`),
  CONSTRAINT `CITY_CODE_FK` FOREIGN KEY (`Cntry_Code`) REFERENCES `country_code` (`Cntry_Code`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city_code`
--

LOCK TABLES `city_code` WRITE;
/*!40000 ALTER TABLE `city_code` DISABLE KEYS */;
INSERT INTO `city_code` VALUES (1,'Delhi',1),(2,'Bangalore',1),(3,'Aus1',2),(4,'Aus2',2),(5,'Pak1',3),(6,'Pak2',3);
/*!40000 ALTER TABLE `city_code` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clue_answer`
--

DROP TABLE IF EXISTS `clue_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clue_answer` (
  `Ans_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Answer` varchar(50) NOT NULL,
  `Answer_CntryID` int(11) NOT NULL,
  `Answer_CityID` int(11) NOT NULL,
  `ANSWER_GEO_MAP_Ans_Geo_ID` int(11) NOT NULL,
  PRIMARY KEY (`Ans_ID`),
  KEY `CLUE_ANSWER_ANSWER_GEO_MAP` (`ANSWER_GEO_MAP_Ans_Geo_ID`),
  KEY `CLUE_ANSWER_CITY_CODE` (`Answer_CityID`),
  KEY `CLUE_ANSWER_COUNTRY_CODE` (`Answer_CntryID`),
  CONSTRAINT `CLUE_ANSWER_ANSWER_GEO_MAP` FOREIGN KEY (`ANSWER_GEO_MAP_Ans_Geo_ID`) REFERENCES `answer_geo_map` (`Ans_Geo_ID`),
  CONSTRAINT `CLUE_ANSWER_CITY_CODE` FOREIGN KEY (`Answer_CityID`) REFERENCES `city_code` (`City_Code`),
  CONSTRAINT `CLUE_ANSWER_COUNTRY_CODE` FOREIGN KEY (`Answer_CntryID`) REFERENCES `country_code` (`Cntry_Code`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clue_answer`
--

LOCK TABLES `clue_answer` WRITE;
/*!40000 ALTER TABLE `clue_answer` DISABLE KEYS */;
INSERT INTO `clue_answer` VALUES (1,'Redfort',1,1,1),(2,'Orin',1,2,1),(3,'Ausplace1',2,3,2),(4,'Ausplace2',2,4,2),(5,'Pakplace1',3,5,3),(6,'Pakplace2',3,6,3);
/*!40000 ALTER TABLE `clue_answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clue_compare`
--

DROP TABLE IF EXISTS `clue_compare`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clue_compare` (
  `CLUE_DATA_Clue_ID` int(11) NOT NULL,
  `User_ID` int(11) NOT NULL,
  `Lvl_reco` int(11) NOT NULL,
  `Relative_lvl` int(11) NOT NULL,
  KEY `CLUE_COMPARE_CLUE_DATA` (`CLUE_DATA_Clue_ID`),
  CONSTRAINT `CLUE_COMPARE_CLUE_DATA` FOREIGN KEY (`CLUE_DATA_Clue_ID`) REFERENCES `clue_data` (`Clue_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clue_compare`
--

LOCK TABLES `clue_compare` WRITE;
/*!40000 ALTER TABLE `clue_compare` DISABLE KEYS */;
/*!40000 ALTER TABLE `clue_compare` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clue_data`
--

DROP TABLE IF EXISTS `clue_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clue_data` (
  `Clue_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Clue` varchar(160) NOT NULL,
  `Clue_Desc` varchar(1000) NOT NULL,
  `Clue_Lvl` decimal(5,2) DEFAULT NULL,
  `User_ID` int(11) NOT NULL,
  `CLUE_VALIDATION_RULE_Rule_ID` int(11) NOT NULL,
  `Clue_Trans_ID` int(11) NOT NULL,
  `CD_CREATED_TS` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `CD_UPDATED_TS` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `CD_DELETED_TS` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`Clue_ID`),
  KEY `CLUE_DATA_CLUE_VALIDATION_RULE` (`CLUE_VALIDATION_RULE_Rule_ID`),
  CONSTRAINT `CLUE_DATA_CLUE_VALIDATION_RULE` FOREIGN KEY (`CLUE_VALIDATION_RULE_Rule_ID`) REFERENCES `clue_validation_rule` (`Rule_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clue_data`
--

LOCK TABLES `clue_data` WRITE;
/*!40000 ALTER TABLE `clue_data` DISABLE KEYS */;
INSERT INTO `clue_data` VALUES (99,'clue8','cluedesc',NULL,100,1,5,'2015-06-05 11:10:33','0000-00-00 00:00:00','0000-00-00 00:00:00'),(100,'clue8','cluedesc',NULL,100,2,5,'2015-06-05 07:50:24','0000-00-00 00:00:00','0000-00-00 00:00:00'),(101,'clue9','cluedesc',NULL,100,3,5,'2015-06-05 11:10:33','0000-00-00 00:00:00','0000-00-00 00:00:00'),(102,'clue10','cluedesc',NULL,100,3,5,'2015-06-05 11:10:33','0000-00-00 00:00:00','0000-00-00 00:00:00'),(114,'aa','awe',4.00,200,2,5,NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `clue_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clue_data_history`
--

DROP TABLE IF EXISTS `clue_data_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clue_data_history` (
  `CDM_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CLUE_DATA_Clue_ID` int(11) NOT NULL,
  `Mod_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Old_Desc` varchar(1000) NOT NULL,
  `Old_Lvl` decimal(5,2) NOT NULL,
  PRIMARY KEY (`CDM_ID`),
  KEY `CLUE_DATA_MODIFICATION_CLUE_DATA` (`CLUE_DATA_Clue_ID`),
  CONSTRAINT `CLUE_DATA_MODIFICATION_CLUE_DATA` FOREIGN KEY (`CLUE_DATA_Clue_ID`) REFERENCES `clue_data` (`Clue_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clue_data_history`
--

LOCK TABLES `clue_data_history` WRITE;
/*!40000 ALTER TABLE `clue_data_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `clue_data_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clue_state_master`
--

DROP TABLE IF EXISTS `clue_state_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clue_state_master` (
  `CLUE_STATE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CLU_STATE_DESC` int(11) NOT NULL,
  PRIMARY KEY (`CLUE_STATE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clue_state_master`
--

LOCK TABLES `clue_state_master` WRITE;
/*!40000 ALTER TABLE `clue_state_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `clue_state_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clue_tag_mapping`
--

DROP TABLE IF EXISTS `clue_tag_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clue_tag_mapping` (
  `CLUE_TAGS_Tag_ID` int(11) NOT NULL,
  `CLUE_DATA_Clue_ID` int(11) NOT NULL,
  KEY `CLUE_TAG_MAPPING_CLUE_DATA` (`CLUE_DATA_Clue_ID`),
  KEY `CLUE_TAG_MAPPING_CLUE_TAGS` (`CLUE_TAGS_Tag_ID`),
  CONSTRAINT `CLUE_TAG_MAPPING_CLUE_DATA` FOREIGN KEY (`CLUE_DATA_Clue_ID`) REFERENCES `clue_data` (`Clue_ID`),
  CONSTRAINT `CLUE_TAG_MAPPING_CLUE_TAGS` FOREIGN KEY (`CLUE_TAGS_Tag_ID`) REFERENCES `clue_tags` (`Tag_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clue_tag_mapping`
--

LOCK TABLES `clue_tag_mapping` WRITE;
/*!40000 ALTER TABLE `clue_tag_mapping` DISABLE KEYS */;
/*!40000 ALTER TABLE `clue_tag_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clue_tags`
--

DROP TABLE IF EXISTS `clue_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clue_tags` (
  `Tag_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Tag` varchar(50) NOT NULL,
  `CLUE_DATA_Clue_ID` int(11) NOT NULL,
  PRIMARY KEY (`Tag_ID`),
  KEY `CLUE_TAGS_CLUE_DATA` (`CLUE_DATA_Clue_ID`),
  CONSTRAINT `CLUE_TAGS_CLUE_DATA` FOREIGN KEY (`CLUE_DATA_Clue_ID`) REFERENCES `clue_data` (`Clue_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clue_tags`
--

LOCK TABLES `clue_tags` WRITE;
/*!40000 ALTER TABLE `clue_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `clue_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clue_trans_state`
--

DROP TABLE IF EXISTS `clue_trans_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clue_trans_state` (
  `CLU_TRANS_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CLUE_DATA_Clue_ID` int(11) NOT NULL,
  `CLU_STATE_ID` int(11) NOT NULL,
  `CLU_TRANS_DATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`CLU_TRANS_ID`),
  KEY `CLUE_TRANS_STATE_CLUE_STATE_MASTER` (`CLU_STATE_ID`),
  KEY `CLUE_TRAS_STATE_CLUE_DATA` (`CLUE_DATA_Clue_ID`),
  CONSTRAINT `CLUE_TRANS_STATE_CLUE_STATE_MASTER` FOREIGN KEY (`CLU_STATE_ID`) REFERENCES `clue_state_master` (`CLUE_STATE_ID`),
  CONSTRAINT `CLUE_TRAS_STATE_CLUE_DATA` FOREIGN KEY (`CLUE_DATA_Clue_ID`) REFERENCES `clue_data` (`Clue_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clue_trans_state`
--

LOCK TABLES `clue_trans_state` WRITE;
/*!40000 ALTER TABLE `clue_trans_state` DISABLE KEYS */;
/*!40000 ALTER TABLE `clue_trans_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clue_validation_data`
--

DROP TABLE IF EXISTS `clue_validation_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clue_validation_data` (
  `Cl_Val_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CLUE_DATA_Clue_ID` int(11) NOT NULL,
  `Data_Makes_Sense` int(11) NOT NULL,
  `Loc_accurate` int(11) NOT NULL,
  `Val_freq` int(11) NOT NULL,
  PRIMARY KEY (`Cl_Val_ID`),
  KEY `Clue_Validation_Data_CLUE_DATA` (`CLUE_DATA_Clue_ID`),
  CONSTRAINT `Clue_Validation_Data_CLUE_DATA` FOREIGN KEY (`CLUE_DATA_Clue_ID`) REFERENCES `clue_data` (`Clue_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clue_validation_data`
--

LOCK TABLES `clue_validation_data` WRITE;
/*!40000 ALTER TABLE `clue_validation_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `clue_validation_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clue_validation_rule`
--

DROP TABLE IF EXISTS `clue_validation_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clue_validation_rule` (
  `Rule_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Rule_Defn` varchar(100) NOT NULL,
  PRIMARY KEY (`Rule_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clue_validation_rule`
--

LOCK TABLES `clue_validation_rule` WRITE;
/*!40000 ALTER TABLE `clue_validation_rule` DISABLE KEYS */;
INSERT INTO `clue_validation_rule` VALUES (1,'hello'),(2,'hi'),(3,'bhanu');
/*!40000 ALTER TABLE `clue_validation_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `country_code`
--

DROP TABLE IF EXISTS `country_code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country_code` (
  `Cntry_Code` int(11) NOT NULL AUTO_INCREMENT,
  `Cntry_Name` varchar(100) NOT NULL,
  PRIMARY KEY (`Cntry_Code`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country_code`
--

LOCK TABLES `country_code` WRITE;
/*!40000 ALTER TABLE `country_code` DISABLE KEYS */;
INSERT INTO `country_code` VALUES (1,'India'),(2,'Aus'),(3,'Pak');
/*!40000 ALTER TABLE `country_code` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-06-10  6:17:05
