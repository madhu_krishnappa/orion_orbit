<!doctype html>
<html data-ng-app="myApp">

<head>
  <meta charset="utf-8">
  <title>NewClue</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  
  <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css"rel="Stylesheet"></link>

<script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

<script type="text/javascript"src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.6/angular.min.js"></script>
 
  <link rel="stylesheet" href="resources/common/css/bootstrap.css" type="text/css">
  <link href="resources/common/css/bootstrap-theme.css" rel="stylesheet" type="text/css">
  <link href="resources/common/css/difficulty level.css" rel="stylesheet" type="text/css">
    <script src="resources/js/bootstrap.min.js"></script>
     <script src="resources/js/newclue.js"></script>
 
</head>

<body style="background-color: white" data-ng-controller="MyController">
  <form data-ng-submit="submit()">
    <script type="text/javascript">
    
       
      function getwords() {
                          var id= document.getElementById("tags2");
                          if(id.value!=''){
       
        				var textbox = document.createElement('input');
        				textbox.setAttribute('type', 'label');

        				textbox.setAttribute("readOnly", "true");

        				document.getElementById('textpost').appendChild(textbox).value = document
        						.getElementById("tags2").value;
                          }
                          id.value='';
                         
      }
    </script>
    <header>
      <div class="navbar navbar-default navbar-fixed-top navbar-inverse">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#example"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
            </button><a href="" class="navbar-brand">My clues</a><a href="" class="navbar-brand">Invite Friend</a><a href="" class="navbar-brand">Share</a>
          </div>
          </div></div>
    </header>
    <br>
    <br>
    <br>
    <br>
    <div class="">
      <div class="container" style="background-color: white">
        <div class="well" style="background-color:white; border-color: transparent; height: 650px">
          <a href="homepage.html"><img src="resources/img/logo1.jpg" width="50px" height="50px">
          </a>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <a href="badges.html"><img src="resources/img/paward.jpg" width="30px" height="30px"> Awards </a>
          <a href="badges.html"><img src="resources/img/baward.png" width="30px" height="30px"> Achievements </a>
          <br>
          <br>
          <br>
          <div id="box" style="background-color:white">
            <div class="alert alert-dismissible alert-success" style="width: 650px; height: 550px; background-color: white; border-color: transparent">
              <div id="drop1">
                <div data-ng-init="getCountry()">
                  <select style="border-radius: 10px; width: 210px; height: 40px" data-ng-model="Countryselected" data-ng-options="country .cntryName for country in getCountries" data-ng-click="getCity()">
                    <option value="">Select Country</option>
                  </select>
                </div>
              </div>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <a href=""><img src="resources/img/Google-Maps-icon.png" alt="" style="width: 75px; height: 75px">
              </a>
              <br>
              <br>
              <div>
                <select style="border-radius: 10px; width: 210px; height: 40px" data-ng-model="cityselect" data-ng-disabled="!Countryselected" data-ng-options=" c.cityName for c in getCities " data-ng-click="getPlace()">
                  <option value="">Select City</option>
                </select>
              </div>
              <div class="" id="faith_voting">
                <fieldset class="rating" id="rating_1" >Difficulty:
               
                  <input data-ng-model="selected_rating"type="radio" id="star7" name="rating" value="7" />
                  <label data-ng-model="selected_rating"for="star7" title="LEVEL 7">7 stars</label>
                  <input data-ng-model="selected_rating"type="radio" id="star6" name="rating" value="6" />
                  <label data-ng-model="selected_rating"for="star6" title="LEVEL 6">6 stars</label>
                  <input data-ng-model="selected_rating"type="radio" id="star5" name="rating" value="5" />
                  <label data-ng-model="selected_rating"for="star5" title="LEVEL 5">5 stars</label>
                  <input data-ng-model="selected_rating"type="radio" id="star4" name="rating" value="4" />
                  <label data-ng-model="selected_rating"for="star4" title="LEVEL 4">4 stars</label>
                  <input data-ng-model="selected_rating"type="radio" id="star3" name="rating" value="3" />
                  <label data-ng-model="selected_rating"for="star3" title="LEVEL 3">3 stars</label>
                  <input data-ng-model="selected_rating" type="radio" id="star2" name="rating" value="2" />
                  <label data-ng-model="selected_rating"for="star2" title="LEVEL 2">2 stars</label>
                  <input data-ng-model="selected_rating"type="radio" id="star1" name="rating" value="1" />
                  <label data-ng-model="selected_rating"for="star1" title="LEVEL 1">1 star</label>
                  </fieldset>
              </div>
              <br>
              <br>
              <input type="text" id="tags" data-ng-model="placeselected" data-ng-keyup="complete()" placeholder="Specific place(Like Taj Mahal not Agra)" style="border: 2px solid grey; border-radius: 5px; width: 210px; height: 40px" />
              <br />
              <br>
              <textarea rows="2" data-ng-model="selectedclue"cols="4" id="Clue" name="Type your clue here" placeholder="Type your clue here" style="border: 2px solid grey; border-radius: 5px; width: 210px; height: 40px" /></textarea>
              <br>
              <br>
              <textarea rows="2" data-ng-model="cluedescription"cols="4" id="Description" name="description" placeholder="Do you want to give a more detailed description/rationale for the clue" style="border: 2px solid grey; border-radius: 5px; width: 210px; height: 45px" /></textarea>
              <br>
              <br>
              <label for="tags2"></label><b><font
        size="4" face="footlight mt light"><b>Tags</b>
                </font>
                </b>
                <input id="tags2" type="text" placeholder="Post Tags" style="border: 2px solid grey; border-radius: 5px; width: 350px; height: 40px" />
                <button onclick="getwords()"class="btn btn-info" style="background-color: light blue">Post</button>
                <p id="para"></p>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <div class="container">
                <div id="textpost"></div>
                  &nbsp;&nbsp;&nbsp;
                  <button type="submit" value="Submit" id="submit"class="btn btn-info" data-toggle="modal" data-target="#modal-1" style="background-color: light blue; border-color: black">Save and Submit</button>
                  &nbsp;&nbsp;&nbsp;&nbsp;
                  <button type="button" class="btn btn-info" style="background-color: light blue; border-color: black">&nbsp;&nbsp;Save for later &nbsp;&nbsp;</button>
                  <div class="modal fade" id="modal-1">
                    <div id="popup2">
                      <div class="modal-dialog modal-lg" style="width: 400px; height: 200px">
                        <div class="modal-content">
                          <div class="modal-header"></div>
                          <div class="modal-body">
                            <div class="navbar navbar-default navbar-fixed-top navbar-inverse">
                              <button type="button" style="color: white" class="close" data-dismiss="modal">&times;</button>
                              <div class="container">
                                <div class="navbar-header">
                                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#example"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                                  </button>
                                  <label class="navbar-brand">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Clue Saved !</label>
                                  <br>
                                  <br>
                                  <label class="navbar-brand">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;If Validated You Will Get +2.2 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;footprint
                                  </label>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  
  
  </form>
</body>

</html>